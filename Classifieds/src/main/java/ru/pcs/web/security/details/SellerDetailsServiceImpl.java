package ru.pcs.web.security.details;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Seller;
import ru.pcs.web.repositories.SellerRepository;

@RequiredArgsConstructor
@Component
public class SellerDetailsServiceImpl implements UserDetailsService {
    private final SellerRepository sellerRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Seller seller = sellerRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("not found"));
        SellerDetailsImpl sellerDetails = new SellerDetailsImpl(seller);
        return sellerDetails;
    }
}
