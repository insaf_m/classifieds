package ru.pcs.web.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class Seller {

    public enum Role {
        ADMIN, USER
    }

    @Enumerated(value = EnumType.STRING)
    public Role role;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String firstName;
    private String lastName;

    @Column(unique = true)
    private String username;

    private String hashpassword;



    @OneToMany(mappedBy = "seller")
    private List<Lots> lots;

}
