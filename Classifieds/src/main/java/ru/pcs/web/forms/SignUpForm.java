package ru.pcs.web.forms;

import lombok.Data;

import javax.persistence.Column;

@Data
public class SignUpForm {
    private String firstName;
    private String lastName;
    private String username;
    private String password;
}
