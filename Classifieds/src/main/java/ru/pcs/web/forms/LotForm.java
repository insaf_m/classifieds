package ru.pcs.web.forms;

import lombok.Data;
import ru.pcs.web.models.Seller;

@Data
public class LotForm {
    private String title;
    private String description;
    private Integer price;
}
