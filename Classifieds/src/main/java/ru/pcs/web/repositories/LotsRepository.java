package ru.pcs.web.repositories;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Lots;
import ru.pcs.web.models.Seller;

import java.util.List;

@Component
public interface LotsRepository extends JpaRepository<Lots, Integer> {
    List<Lots> findAll();
    List<Lots> findAllBySeller_Id(Integer sellerId);

}
