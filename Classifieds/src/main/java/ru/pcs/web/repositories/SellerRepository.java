package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Seller;

import java.util.Optional;

public interface SellerRepository extends JpaRepository<Seller, Integer> {
Optional<Seller> findByUsername(String username);
}
