package ru.pcs.web.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.SignUpForm;
import ru.pcs.web.models.Seller;
import ru.pcs.web.repositories.SellerRepository;

@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {

    private final PasswordEncoder passwordEncoder;
    private final SellerRepository sellerRepository;
    @Override
    public void signUpSeller(SignUpForm form) {
        Seller seller = Seller.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .username(form.getUsername())
                .role(Seller.Role.USER)
                .hashpassword(passwordEncoder.encode(form.getPassword()))
                .build();
        sellerRepository.save(seller);

    }
}
