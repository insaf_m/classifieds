package ru.pcs.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.LotForm;
import ru.pcs.web.models.Lots;
import ru.pcs.web.models.Seller;
import ru.pcs.web.repositories.LotsRepository;
import ru.pcs.web.repositories.SellerRepository;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;

@Component
public class LotsServiceImpl implements LotsService {

    private final LotsRepository lotsRepository;
    private final SellerRepository sellerRepository;

    @Autowired
    public LotsServiceImpl(LotsRepository lotsRepository,SellerRepository sellerRepository) {
        this.lotsRepository = lotsRepository;
        this.sellerRepository=sellerRepository;
    }

    @Override
    public void addLot(LotForm form) {
        Lots lot = Lots.builder()
                .title(form.getTitle())
                .description(form.getDescription())
                .price(form.getPrice())
                .build();
        lotsRepository.save(lot);
    }

    @Override
    public List<Lots> getAllLots() {
        return lotsRepository.findAll();
    }


    @Override
    public void deleteLot(Integer lotId) {
        lotsRepository.deleteById(lotId);
    }

    @Override
    public Lots getLot(Integer lotId) {
        return lotsRepository.findById(lotId).orElseThrow();
    }

    @Override
    public void update(Integer lotId, LotForm lotForm) {
        Lots lot = lotsRepository.getById(lotId);
        lot.setTitle(lotForm.getTitle());
        lot.setDescription(lotForm.getDescription());
        lot.setPrice(lotForm.getPrice());
        lotsRepository.save(lot);
    }

    @Override
    public List<Lots> getLotsBySeller(Integer sellerId) {
        return lotsRepository.findAllBySeller_Id(sellerId);
    }

    @Override
    public void addLotOfSeller(LotForm form, Integer sellerId) {
        Lots lot = Lots.builder()
                .title(form.getTitle())
                .description(form.getDescription())
                .price(form.getPrice())
                .build();
        Seller seller = sellerRepository.getById(sellerId);
        lot.setSeller(seller);
        lotsRepository.save(lot);
    }
}
