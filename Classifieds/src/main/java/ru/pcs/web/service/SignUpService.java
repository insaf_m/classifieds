package ru.pcs.web.service;

import ru.pcs.web.forms.SignUpForm;

public interface SignUpService {
    void signUpSeller(SignUpForm form);
}
