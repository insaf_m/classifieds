package ru.pcs.web.service;

import ru.pcs.web.forms.LotForm;
import ru.pcs.web.models.Lots;

import java.util.List;

public interface LotsService {
    void addLot(LotForm form);
    List<Lots> getAllLots();
    void deleteLot(Integer lotId);
    Lots getLot(Integer lotId);
    void update(Integer lotId, LotForm lotForm);
    List<Lots> getLotsBySeller(Integer sellerId);

    void addLotOfSeller(LotForm form, Integer sellerId);
}
