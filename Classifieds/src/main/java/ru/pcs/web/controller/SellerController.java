package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.forms.LotForm;
import ru.pcs.web.models.Lots;
import ru.pcs.web.repositories.LotsRepository;
import ru.pcs.web.service.LotsService;

import java.util.List;


@Controller
public class SellerController {

    private final LotsService lotsService;

    @Autowired
    public SellerController(LotsService lotsService) {
        this.lotsService = lotsService;
    }

    @GetMapping("/lots")
    public String getLotsPage(Model model) {
        List<Lots> lots = lotsService.getAllLots();
        model.addAttribute("lots", lots);
        return "lots";
    }


    @PostMapping("/lots/add")
    public String addLot(LotForm form){

        lotsService.addLot(form);

        return "redirect:/lots";
    }

    @PostMapping("/lots/{lot-id}/delete")
    public  String deleteLot(@PathVariable("lot-id") Integer lotId) {
        lotsService.deleteLot(lotId);
        return "redirect:/lots";
    }

    @PostMapping("/lots/{lot-id}/update")
    public  String update(@PathVariable("lot-id") Integer lotId, LotForm lotForm) {
        lotsService.update(lotId, lotForm);
        return "redirect:/lots";
    }

    @GetMapping("/lots/{lot-id}")
    public String getLotPage(Model model, @PathVariable("lot-id") Integer lotId) {
        Lots lot = lotsService.getLot(lotId);
        model.addAttribute("lot", lot);
        return "lot";
    }



    @GetMapping("/seller/{seller-id}/lots")
    public String getLotsBySeller(@PathVariable("seller-id") Integer sellerId, Model model) {
        List<Lots> lots = lotsService.getLotsBySeller(sellerId);
        model.addAttribute("lotsOfSeller", lots);
        model.addAttribute("sellerId", sellerId);
        return "seller_lots";

    }

    @PostMapping("/seller/{seller-id}/lots")
    public String addLotFromSeller(@PathVariable("seller-id") Integer sellerId, LotForm form){
        lotsService.addLotOfSeller(form,sellerId);
        return "redirect:/seller/" + sellerId + "/lots";
    }


}
